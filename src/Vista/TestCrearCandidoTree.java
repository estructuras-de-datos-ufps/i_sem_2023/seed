/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.ArbolB;
import Util.seed.NodoBin;
import Util.seed.BTreePrinter;
import java.util.Iterator;

/**
 *
 * @author DOCENTE
 */
public class TestCrearCandidoTree {

    public static void main(String[] args) {
        NodoBin<Integer> n1 = new NodoBin(8);
        NodoBin<Integer> n2 = new NodoBin(3);
        NodoBin<Integer> n3 = new NodoBin(10);
        NodoBin<Integer> n4 = new NodoBin(1);
        NodoBin<Integer> n5 = new NodoBin(6);
        NodoBin<Integer> n6 = new NodoBin(14);
        NodoBin<Integer> n7 = new NodoBin(4);
        NodoBin<Integer> n8 = new NodoBin(7);
        NodoBin<Integer> n9 = new NodoBin(13);
        ArbolB<Integer> t = new ArbolB();
        t.setRaiz(n1);
        //Configurar las ramas:
        n1.setIzq(n2);
        n1.setDer(n3);
        n2.setIzq(n4);
        n2.setDer(n5);
        n5.setIzq(n7);
        n5.setDer(n8);
        n3.setDer(n6);
        n6.setIzq(n9);
        BTreePrinter.printNode(t.getRaiz());
        System.out.println("Cantidad nodos ramas:" + t.getCantidadNodosRamas());
        imprimir("Nodos ramas", t.getInfoNodosRamas());
        imprimir("Pre:", t.getPreOrden());
        imprimir("In:", t.getInOrden());
        imprimir("Post:", t.getPosOrden());
        imprimir("Hojas Izq:", t.getHojasIzq());

    }

    private static void imprimir(String title, Iterator<Integer> it) {
        System.out.println(title);
        while (it.hasNext()) {
            System.out.print(it.next()+"\t");
        }
        System.out.println("");
    }
}
