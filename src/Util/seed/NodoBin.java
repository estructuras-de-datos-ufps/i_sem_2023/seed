/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

/**
 *
 * @author DOCENTE
 */
public class NodoBin<T> {
    
    private T info;
    private NodoBin<T> izq, der;

    public NodoBin(T info) {
        this.info = info;
    }

    public NodoBin(T info, NodoBin<T> izq, NodoBin<T> der) {
        this.info = info;
        this.izq = izq;
        this.der = der;
    }

    
    
    public NodoBin() {
    }

    public T getInfo() {
        return info;
    }

    public void setInfo(T info) {
        this.info = info;
    }

    public NodoBin<T> getIzq() {
        return izq;
    }

    public void setIzq(NodoBin<T> izq) {
        this.izq = izq;
    }

    public NodoBin<T> getDer() {
        return der;
    }

    public void setDer(NodoBin<T> der) {
        this.der = der;
    }

    @Override
    public String toString() {
        return "(" + info + ')';
    }
    
    public boolean isHoja()
    {
        return this.izq==null && this.der==null;
    }
}
