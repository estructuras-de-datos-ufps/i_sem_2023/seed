/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

import java.util.Iterator;

/**
 *
 * @author DOCENTE
 */
public class ArbolB<T> {

    private NodoBin<T> raiz;

    public ArbolB() {
    }

    public NodoBin<T> getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoBin<T> raiz) {
        this.raiz = raiz;
    }

    //Decorador:
    public int getCantidadNodosRamas() {
        this.existeArbol();
        return this.getCantidadNodosRamas(raiz);
    }

    //Lógica del método recursivo:
    private int getCantidadNodosRamas(NodoBin<T> r) {
        if (r == null) {
            return 0;
        }
        int c = (r.isHoja()) ? 0 : 1;
        return c + this.getCantidadNodosRamas(r.getIzq()) + this.getCantidadNodosRamas(r.getDer());
    }

    public Iterator<T> getInfoNodosRamas() {
        this.existeArbol();
        ListaCD<T> l = new ListaCD();

        //Paso por referencia
        this.getInfoRamas(raiz, l);
        return l.iterator();
    }

    private void getInfoRamas(NodoBin<T> r, ListaCD<T> l) {
        if (r == null) {
            return;
        }
        if (!r.isHoja()) {
            l.insertarFin(r.getInfo());
        }
        this.getInfoRamas(r.getIzq(), l);
        this.getInfoRamas(r.getDer(), l);
    }

    private void existeArbol() {
        if (this.raiz == null) {
            throw new RuntimeException("Arbol vacío");
        }
    }

    public Iterator<T> getPreOrden() {
        this.existeArbol();
        ListaCD<T> l = new ListaCD();
        this.getPreOrden(raiz, l);
        return l.iterator();
    }

    private void getPreOrden(NodoBin<T> r, ListaCD<T> l) {
        if (r == null) {
            return;
        }

        l.insertarFin(r.getInfo());
        this.getPreOrden(r.getIzq(), l);
        this.getPreOrden(r.getDer(), l);
    }
    
    
    public Iterator<T> getInOrden() {
        this.existeArbol();
        ListaCD<T> l = new ListaCD();
        this.getInOrden(raiz, l);
        return l.iterator();
    }

    private void getInOrden(NodoBin<T> r, ListaCD<T> l) {
        if (r == null) {
            return;
        }

        
        this.getInOrden(r.getIzq(), l);
        l.insertarFin(r.getInfo());
        this.getInOrden(r.getDer(), l);
    }
    
    
    public Iterator<T> getPosOrden() {
        this.existeArbol();
        ListaCD<T> l = new ListaCD();
        this.getPosOrden(raiz, l);
        return l.iterator();
    }

    private void getPosOrden(NodoBin<T> r, ListaCD<T> l) {
        if (r == null) {
            return;
        }
      
        this.getPosOrden(r.getIzq(), l);
        this.getPosOrden(r.getDer(), l);
        l.insertarFin(r.getInfo());
    }
    
    
    
    
    public Iterator<T> getHojasIzq() {
        this.existeArbol();
        ListaCD<T> l = new ListaCD();
        this.getHojasIzq(raiz, l,false);
        return l.iterator();
    }
    
    private void getHojasIzq(NodoBin<T> r, ListaCD<T> l,boolean izq) {
        if (r == null) {
            return;
        }
        if(izq && r.isHoja())
            l.insertarFin(r.getInfo());
        this.getHojasIzq(r.getIzq(), l,true);
        this.getHojasIzq(r.getDer(), l,false);
    }
    
    
}
